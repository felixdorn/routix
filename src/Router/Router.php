<?php


namespace Felix\Router;

use BadFunctionCallException;
use Closure;
use Felix\Router\Exceptions\InvalidMethodException;
use Felix\Router\Exceptions\UrlGeneratorException;
use GuzzleHttp\Psr7\Response;
use League\Route\Http\Exception\MethodNotAllowedException;
use League\Route\Http\Exception\NotFoundException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Router
 * @package Felix\Router
 * @method Router get(string $path, $handler, string $name = null) Map a get request
 * @method Router post(string $path, $handler, string $name = null) Map a post request
 * @method Router put(string $path, $handler, string $name = null) Map a put request
 * @method Router patch(string $path, $handler, string $name = null) Map a patch request
 * @method Router delete(string $path, $handler, string $name = null) Map a delete request
 * @method Router head(string $path, $handler, string $name = null) Map a head request
 * @method Router options(string $path, $handler, string $name = null) Mao a options request
 */
class Router
{
    public const error404 = 404;
    public const error405 = 405;
    public const METHODS = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'];

    /**
     * @var ContainerInterface|null
     */
    private $container;

    /**
     * @var LoggerInterface|null
     */
    private $logger;

    /**
     * @var LeagueRouter
     */
    private $router;

    /**
     * @var Route[]
     */
    private $namedRoutes;

    /**
     * @var Route[]
     */
    private $routes;

    /**
     * @var bool
     */
    private $removeTrailingSlash;

    /**
     * @var bool
     */
    private $moveToHttps;
    /**
     * @var MiddlewareInterface[]
     */
    private $middlewares;

    public function __construct(LeagueRouter $router, ?ContainerInterface $container = null, ?LoggerInterface $logger = null)
    {
        $this->container = $container;
        $this->logger = $logger;
        $this->router = $router;
        $this->namedRoutes = [];
        $this->middlewares = [];
        $this->routes = [];
    }

    /**
     * @param bool $removeTrailingSlash
     * @return Router
     */
    public function setRemoveTrailingSlash(bool $removeTrailingSlash): Router
    {
        $this->removeTrailingSlash = $removeTrailingSlash;
        return $this;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface|int
     */
    public function match(ServerRequestInterface $request)
    {
        $normalizedRequest = $this->normalizeRequest($request);
        if ($normalizedRequest instanceof ResponseInterface) {
            return $normalizedRequest;
        }

        $this->registerRoutes($this->routes);
        $this->router->addRoutes($this->routes);

        $this->router->middlewares($this->middlewares);
        try {
            return $this->router->dispatch($request);
        } catch (NotFoundException $exception) {
            if ($this->logger) {
                $this->logger->warning("Path " . $request->getUri()->getPath() . " returned a 404.");
            }
            return $this::error404;
        } catch (MethodNotAllowedException $exception) {
            if ($this->logger) {
                $this->logger->warning("Path " . $request->getUri()->getPath() . " returned a 405.");
            }
            return $this::error405;
        }
    }

    private function normalizeRequest(ServerRequestInterface $request)
    {
        if ($this->removeTrailingSlash && strlen($request->getUri()->getPath()) > 1 && rtrim($request->getUri()->getPath(), '/') !== $request->getUri()->getPath()) {
            if ($this->logger) {
                $this->logger->info("A request was redirected to it's equivalent without the trailing slash", [$request]);
            }
            return new Response(302, ['Location' => rtrim($request->getUri()->getPath(), '/')]);
        }

        if ($this->moveToHttps && $request->getUri()->getScheme() === 'http') {
            if ($this->logger) {
                $this->logger->info("A request was moved from http to https scheme.", [$request]);
            }
            return new Response(302, ['Location' => str_replace('http://', 'https://', $request->getUri())]);
        }

        return false;
    }

    private function registerRoutes(array $routes)
    {
        $normalized = [];
        /** @var Route $route */
        foreach ($routes as $route) {
            foreach ($route->compile() as $route) {
                $normalized[] = $route;
            }
        }
        $this->routes = $normalized;
    }

    /**
     * @param string|array $methods
     * @param string $path
     * @param Closure|string $handler
     * @param string $name
     * @return Route
     * @throws InvalidMethodException
     */
    public function map($methods, string $path, $handler, string $name = null)
    {
        $methods = $this->normalizeMethods($methods);
        $path = $this->normalizePath($path);
        $handler = $this->normalizeHandler($handler);

        $route = new Route($methods, $path, $handler, $name);

        $this->routes[] = $route;

        if ($route->hasName()) {
            $this->namedRoutes[$route->getName()] = $route;
        }

        return $route;
    }

    /**
     * @param $methods
     * @return array
     * @throws InvalidMethodException
     */
    private function normalizeMethods($methods)
    {
        if (is_array($methods)) {
            return $methods;
        }

        if (strpos($methods, ',') !== false) {
            $methods = explode(',', $methods);
        }

        if (is_string($methods) && strpos($methods, '|') !== false) {
            $methods = explode('|', $methods);
        }

        if (is_string($methods)) {
            $methods = [$methods];
        }

        $isValid = (count(array_intersect(self::METHODS, $methods))) ? true : false;

        if ($isValid !== false) {
            return $methods;
        }


        throw new InvalidMethodException($methods);
    }

    /**
     * @param string $path
     * @return bool|string
     */
    private function normalizePath(string $path)
    {
        if (substr($path, -1) === '/') {
            return substr($path, 0, -1);
        }

        return $path;
    }

    /**
     * @param $handler
     * @return mixed|string
     */
    private function normalizeHandler($handler)
    {
        if (is_array($handler) && count($handler) === 2) {
            return $handler[0] . '::' . $handler[1];
        }

        if (is_string($handler)) {
            if (strpos($handler, '::') !== false) {
                return $handler;
            }

            if (strpos($handler, '@') !== false) {
                return str_replace('@', '::', $handler);
            }

            if (strpos($handler, '->') !== false) {
                return str_replace('->', '::', $handler);
            }
        }

        return $handler;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|string
     * @throws UrlGeneratorException
     */
    public function generate(string $name, array $arguments = [])
    {
        if (array_key_exists($name, $this->namedRoutes) === false) {
            throw new UrlGeneratorException("Route \"$name\" doesn't exists. Maybe $name has a typo ?");
        }

        $path = $this->namedRoutes[$name]->getPath();

        if ($arguments === []) {
            return $path;
        }

        foreach ($arguments as $name => $argument) {
            $path = str_replace("{{$name}}", urlencode($argument), $path);
        }

        return $path;
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {

        $name = strtoupper($name);

        if (in_array($name, $this::METHODS) === false) {
            throw new BadFunctionCallException("Can't map a route for method \"$name\".");
        }

        // To put the method at the right place.
        foreach ($arguments as $index => $argument) {
            $arguments[$index + 1] = $argument;
            if ($index === 0) {
                $arguments[0] = $name;
            }
        }

        return call_user_func_array([$this, 'map'], $arguments);
    }

    /**
     * @param MiddlewareInterface[] $middlewares
     * @return Router
     */
    public function setMiddlewares(array $middlewares): Router
    {
        $this->middlewares = $middlewares;
        return $this;
    }

    /**
     * @param bool $moveToHttps
     * @return $this
     */
    public function moveToHttps(bool $moveToHttps = true)
    {
        $this->moveToHttps = $moveToHttps;

        return $this;
    }
}
