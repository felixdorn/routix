<?php


namespace Felix\Router;

use League\Route\Route as LeagueRoute;
use Psr\Http\Server\MiddlewareInterface;
use SuperClosure\Serializer;

class Route
{
    /**
     * @var array
     */
    protected $methods;
    /**
     * @var string
     */
    protected $path;
    /**
     * @var string|callable
     */
    protected $handler;
    /**
     * @var int
     */
    private $port;
    /**
     * @var string
     */
    private $scheme;
    /**
     * @var string
     */
    private $host;
    /**
     * @var MiddlewareInterface[]
     */
    private $middlewares;
    /**
     * @var string
     */
    private $name;


    /**
     * Route constructor.
     * @param array $methods
     * @param string $path
     * @param $handler
     * @param string|null $name
     */
    public function __construct(array $methods, string $path, $handler, ?string $name = null)
    {

        $this->methods = $methods;
        $this->path = $path;
        $this->handler = $handler;
        $this->scheme = null;
        $this->host = null;
        $this->port = null;
        $this->middlewares = [];
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getMethods(): array
    {
        return $this->methods;
    }

    /**
     * @param array $methods
     * @return Route
     */
    public function setMethods(array $methods): Route
    {
        $this->methods = $methods;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Route
     */
    public function setName(string $name): Route
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return Route
     */
    public function setPath(string $path): Route
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @param mixed $handler
     * @return Route
     */
    public function setHandler($handler)
    {
        $this->handler = $handler;
        return $this;
    }

    /**
     * @param int $port
     * @return Route
     */
    public function setPort(int $port): Route
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @param string $scheme
     * @return Route
     */
    public function setScheme(string $scheme): Route
    {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     * @param string $host
     * @return Route
     */
    public function setHost(string $host): Route
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return LeagueRoute[]
     */
    public function compile(): array
    {
        $routes = [];
        foreach ($this->methods as $method) {
            $route = new LeagueRoute($method, empty($this->path) ? '/' : $this->path, $this->handler);
            if ($this->host) {
                $route->setHost($this->host);
            }
            if ($this->port) {
                $route->setPort($this->port);
            }
            if ($this->scheme) {
                $route->setScheme($this->scheme);
            }
            if ($this->middlewares !== []) {
                $route->middlewares($this->middlewares);
            }
            $routes[] = $route;
        }

        return $routes;
    }

    /**
     * @param MiddlewareInterface $middleware
     * @return Route
     */
    public function middleware(MiddlewareInterface $middleware): Route
    {
        $this->middlewares[] = $middleware;
        return $this;
    }

    /**
     * @param array $middlewares
     * @return Route
     */
    public function middlewares(array $middlewares): Route
    {
        $this->middlewares = $middlewares;
        return $this;
    }

    public function hasName(): bool
    {
        return empty($this->name) === false;
    }
}
