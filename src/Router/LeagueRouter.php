<?php


namespace Felix\Router;

class LeagueRouter extends \League\Route\Router
{
    public function addRoutes(array $routes)
    {
        foreach ($routes as $route) {
            $this->routes[] = $route;
        }

        return $this;
    }
}
