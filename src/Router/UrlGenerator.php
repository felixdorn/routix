<?php


namespace Felix\Router;

class UrlGenerator
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return mixed|string
     * @throws Exceptions\UrlGeneratorException
     */
    public function generate(string $name, array $arguments = [])
    {
        return $this->router->generate($name, $arguments);
    }
}
