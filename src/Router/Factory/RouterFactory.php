<?php

namespace Felix\Router\Factory;

use Doctrine\Annotations\AnnotationReader;
use Doctrine\Common\Cache\CacheProvider;
use Felix\Router\AnnotationRouter;
use Felix\Router\Annotations\Route;
use Felix\Router\Exceptions\InvalidMethodException;
use Felix\Router\LeagueRouter;
use Felix\Router\Router;
use Felix\Router\Utils;
use League\Route\Strategy\ApplicationStrategy;
use Psr\Http\Server\MiddlewareInterface;
use ReflectionMethod;

class RouterFactory extends RouterFactorySetters
{
    /**
     * @var LeagueRouter
     */
    private $router;
    /**
     * @var Route[]
     */
    private $discoveredRoutes;
    /**
     * @var bool
     */
    private $removeTrailingSlash;
    /**
     * @var bool
     */
    private $moveToHttps;
    /**
     * @var MiddlewareInterface[]
     */
    private $middlewares;
    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * RouterFactory constructor.
     */
    public function __construct()
    {
        $this->container = null;
        $this->logger = null;
        $this->defaultsHeaders = [];
        $this->discoveredRoutes = [];
        $this->removeTrailingSlash = true;
        $this->moveToHttps = true;
        $this->middlewares = [];
        $this->cache = null;
    }

    /**
     * @param CacheProvider $cache
     * @return RouterFactory
     */
    public function setCache(CacheProvider $cache): RouterFactory
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return Router
     * @throws InvalidMethodException
     */
    public function build()
    {
        $this->router = $this->setRouter();

        $router = new Router($this->router, $this->container, $this->logger);

        $router->setRemoveTrailingSlash($this->removeTrailingSlash);

        $router->moveToHttps($this->moveToHttps);

        if ($this->middlewares !== []) {
            $router->setMiddlewares($this->middlewares);
        }
        foreach ($this->discoveredRoutes as $discoveredRoute) {
            $route = $router->map($discoveredRoute->methods, $discoveredRoute->path, $discoveredRoute->getHandler());
            $middlewares = array_merge($discoveredRoute->after, $discoveredRoute->before);
            if ($middlewares === []) {
                continue;
            }
            $instancedMiddlewares = [];
            foreach ($middlewares as $middleware) {
                $instancedMiddlewares[] = new $middleware();
            }

            $route->middlewares($instancedMiddlewares ?? []);
        }


        return $router;
    }

    /**
     * @return LeagueRouter
     */
    private function setRouter()
    {
        $strategy = new ApplicationStrategy();

        if ($this->container !== null) {
            $strategy->setContainer($this->container);
        }

        if ($this->defaultsHeaders !== []) {
            $strategy->addDefaultResponseHeaders($this->defaultsHeaders);
        }


        $router = new LeagueRouter();


        $router->setStrategy($strategy);


        return $router;
    }

    /**
     * @param AnnotationRouter $annotations
     * @return RouterFactory
     */
    public function setAnnotationHandler(AnnotationRouter $annotations)
    {
        if ($this->cache && $this->cache->contains('route.annotations')) {
            $this->discoveredRoutes = $this->cache->fetch('route.annotations');
            return $this;
        }

        $methods = $annotations->getReflectedMethods();
        $reader = new AnnotationReader();

        $routes = [];

        /** @var ReflectionMethod $method */
        foreach ($methods as $method) {

            /** @var Route[] $route */
            $route = $reader->getMethodAnnotations($method);

            foreach ($route as $declaredRoute) {
                $declaredRoute->setHandler($method->getDeclaringClass()->getName() . '::' . $method->getName());
                $routes[] = $declaredRoute;
            }
        }

        $routes = Utils::array_flatten($routes);

        $this->discoveredRoutes = $routes;

        if ($this->cache) {
            $this->cache->save('route.annotations', $routes);
        }
        return $this;
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function removeTrailingSlashIfExist(bool $value = true)
    {
        $this->removeTrailingSlash = $value;

        return $this;
    }

    /**
     * @param bool $value
     * @return $this
     */
    public function moveToHttps(bool $value = true)
    {
        $this->moveToHttps = $value;

        return $this;
    }

    public function addDefaultMiddleware(MiddlewareInterface $middleware)
    {
        $this->middlewares[] = $middleware;

        return $this;
    }

    public function addDefaultMiddlewares(array $middlewares)
    {
        $this->middlewares = array_merge($this->middlewares, $middlewares);

        return $this;
    }
}
