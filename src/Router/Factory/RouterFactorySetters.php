<?php


namespace Felix\Router\Factory;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class RouterFactorySetters
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var string[]
     */
    protected $defaultsHeaders;

    /**
     * @param string[] $defaultsHeaders
     * @return RouterFactory
     */
    public function addDefaultsHeaders(array $defaultsHeaders): self
    {
        $this->defaultsHeaders = array_merge($defaultsHeaders, $this->defaultsHeaders);
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return RouterFactory
     */
    public function addDefaultsHeader(string $key, string $value): self
    {
        $this->defaultsHeaders[$key] = $value;
        return $this;
    }

    /**
     * @param ContainerInterface $container
     * @return RouterFactory
     */
    public function setContainer(ContainerInterface $container = null): self
    {
        $this->container = $container;
        return $this;
    }

    /**
     * @param LoggerInterface $logger
     * @return RouterFactory
     */
    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;
        return $this;
    }
}
