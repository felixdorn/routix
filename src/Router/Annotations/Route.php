<?php


namespace Felix\Router\Annotations;

use Doctrine\Annotations\Annotation\Required;

/**
 * Class Route
 * @package Felix\Router\Annotations
 * @Annotation
 */
class Route
{
    public function __construct()
    {
        $this->before = [];
        $this->after = [];
    }

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     * @Required()
     */
    public $path;

    /**
     * @var array
     * @Required()
     */
    public $methods;
    /**
     * @var array
     */
    public $before;
    /**
     * @var array
     */
    public $after;
    /**
     * @var string
     */
    private $handler;

    /**
     * @return mixed
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @param mixed $handler
     * @return Route
     */
    public function setHandler($handler)
    {
        $this->handler = $handler;
        return $this;
    }
}
