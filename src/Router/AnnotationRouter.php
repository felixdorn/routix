<?php

namespace Felix\Router;

use ReflectionException;
use function array_merge;
use function func_get_args;
use function is_array;

class AnnotationRouter
{
    /**
     * @var string|string[]
     */
    private $paths;

    /**
     * AnnotationRouter constructor.
     * @param string[]|string $paths
     */
    public function __construct($paths)
    {

        $files = [];

        if (is_string($paths)) {
            $paths = [$paths];
        }


        foreach ($paths as $i => $path) {
            if (substr($path, -1) !== '/') {
                $paths[$i] = $path . '/';
            }

            $files[] = $this->getFiles($path);
        }


        $this->paths = $files;
    }

    /**
     * @param string $directory
     * @param array $results
     * @return array
     */
    private function getFiles(string $directory, &$results = [])
    {
        $files = scandir($directory);

        foreach ($files as $key => $value) {
            $path = realpath($directory . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } elseif ($value != "." && $value != "..") {
                $this->getFiles($path, $results);
                $results[] = $path;
            }
        }

        return Utils::array_flatten($results);
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    public function getReflectedMethods()
    {
        $paths = $this->getPaths();
        $methods = [];

        foreach ($paths as $path) {
            $class = $this->getClassName($path);
            $reflection = new \ReflectionClass($class);
            $methods[] = $reflection->getMethods();
        }

        return Utils::array_flatten($methods);
    }

    /**
     * @return string|string[]
     */
    public function getPaths()
    {
        return Utils::array_flatten($this->paths);
    }

    /**
     * @param string $file
     * @return string
     */
    private function getClassName(string $file)
    {
        $fp = fopen($file, 'r');
        $class = $namespace = $buffer = '';
        $i = 0;
        while (!$class) {
            if (feof($fp)) {
                break;
            }

            $buffer .= fread($fp, 512);
            $tokens = token_get_all($buffer);

            if (strpos($buffer, '{') === false) {
                continue;
            }

            for (; $i < count($tokens); $i++) {
                if ($tokens[$i][0] === T_NAMESPACE) {
                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j][0] === T_STRING) {
                            $namespace .= '\\' . $tokens[$j][1];
                        } elseif ($tokens[$j] === '{' || $tokens[$j] === ';') {
                            break;
                        }
                    }
                }

                if ($tokens[$i][0] === T_CLASS) {
                    for ($j = $i + 1; $j < count($tokens); $j++) {
                        if ($tokens[$j] === '{') {
                            $class = $tokens[$i + 2][1];
                        }
                    }
                }
            }
        }

        return $namespace . '\\' . $class;
    }
}
