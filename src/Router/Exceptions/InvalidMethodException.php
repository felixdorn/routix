<?php


namespace Felix\Router\Exceptions;

use Exception;
use Throwable;

class InvalidMethodException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (is_array($message)) {
            $message = implode(' and ', $message);
        }
        parent::__construct("Provided methods can't be resolved. A typo ? ($message)", $code, $previous);
    }
}
