<?php

use Controllers\HomeController;
use DI\Container;
use Doctrine\Common\Cache\FilesystemCache;
use Felix\Router\AnnotationRouter;
use Felix\Router\Factory\RouterFactory;
use Felix\Router\Router;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use function Http\Response\send;

require '../vendor/autoload.php';
require __DIR__ . '/SomeMiddleware.php';
ErrorHandler::register();
ExceptionHandler::register();

$annotation = new AnnotationRouter(__DIR__ . '/Controllers/');

$router = (new RouterFactory)
    ->setContainer(new Container)
    ->addDefaultsHeader('some', 'header')
    ->addDefaultsHeaders(['like' => 'that'])
    ->setCache(new FilesystemCache(__DIR__ . '/cache'))
    ->removeTrailingSlashIfExist()
    ->addDefaultMiddleware(new SomeMiddleware())
    ->moveToHttps(false)
    ->setAnnotationHandler($annotation)
    ->build();


$router->map('GET', '/complex/{arguments}', function () use ($router) {
    return new Response(200, [], $router->genera-te('some.complex', ['arguments' => "my-slug"]));
}, 'some.complex')->setScheme('http');

$router->map('GET', '/', function () use ($router) {
    return new Response(200, [], $router->generate('some.complex', ['arguments' => 'some-slug']));
});

$router->get('/invoke/magic/method', HomeController::class);

$router->get('/method/in/controller', '\Controllers\HomeController->index');


$res = $router->match(ServerRequest::fromGlobals());

if ($res === Router::error404) {
    die('Error 404');
}


if ($res === Router::error405) {
    die('Error 405');
}
send($res);
