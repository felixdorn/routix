<?php


namespace Controllers;

use Felix\Router\Annotations\Route;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeController
{
    /**
     * @param ServerRequestInterface $request
     * @param mixed[] $options
     * @return ResponseInterface
     * @Route(path="/s", methods={"GET", "POST"})
     */
    public function index(ServerRequestInterface $request, array $options): ResponseInterface
    {
        return new Response('200', [], "Hello world!!");
    }

    /**
     * @param ServerRequestInterface $request
     * @param mixed[] $options
     * @return ResponseInterface
     * @Route(path="/this-is/my-path",methods={"GET"})
    */
    public function __invoke(ServerRequestInterface $request, array $options): ResponseInterface
    {
        return new Response('200', [], "Hello world");
    }

}
